#ifndef VECTOR_H
#define VECTOR_H
#include <cassert>
#include <typeinfo>

template<typename T>
class  Vector  {
public:
	typedef T value_type;

	//set all elements to zero
	Vector()
	    : size_(0)
	{}

	//allocate number of row col and elements
	Vector(int size)
	    : size_(size),data_(size)
	{}

	// copy constructor
	Vector(const Vector<T>& m) {
		size_=m.size_;
		data_=m.data_;
	}

	/*
	 * ***********
	 *  Functions in Class Matrix ------
	 *  ***********
	*/

	int size() {
		return size_;
	} // ----------

	void fill(T val) {
		std::fill(data_.begin(),data_.end(),val);
	} // ----------

	void pushback(const T& in) {
		size_=size_+1;
		data_.push_back(in);
	} // ----------

	void resize(int size) {
		assert(size>0);
		size_=size;
		data_.resize(size);
		clear();
	} // ----------

	T& operator()(int i){
		assert(size_==data_.size());
		assert(i<size_);
		return data_[i];
	} // ----------

	void print(){
		std::cout.precision(6);
		std::cout<<"size:= "<<size_<<std::endl;
		for(int i=0; i<size_; i++) {
			std::cout << data_[i] << "\t";
		}
		std::cout << std::endl;
		return;
	} // ----------

	void clear(){
		T zero = T(0.0);
		fill(0.0);
		return;
	} // ----------

	T dot(Vector<T>& A){
		assert(size_==A.size());
		T out = T(0);
		for(int i=0; i<size_; i++) {
			out = out + conj(data_[i])*A(i);
			//out += data_[i]*conj(A(i));
		}
		return out;
	} // ----------


//	Vector<T> mult(const T& in) {
//		Vector<T> out(size_);
//		for (int i=0;i<size_;i++)
//			out(i) = in*data_[i];
//		return out;
//	} // ----------


//	Vector<T>& minus(Vector<T>& Vin) {
//		assert(size_==Vin.size());
//		Vector<T> out(size_);
//		for (int i=0;i<size_;i++)
//			out(i) = data_[i] - Vin(i);
//		return out;
//	} // ----------


	void normalize(){

		if(std::is_same<T, std::complex<double>>::value ||
		        std::is_same<T, std::complex<int>>::value    ||
		        std::is_same<T, std::complex<float>>::value) {
			T out = T(0);
			for(int i=0; i<size_; i++) {
				out = out + conj(data_[i])*data_[i];
			}
			out = sqrt(out);
			for(int i=0; i<size_; i++) {
				data_[i] = data_[i] * (1.0/out);
			}
		} else {
			T out = T(0);
			for(int i=0; i<size_; i++) {
				out = out + data_[i]*data_[i];
			}
			out = sqrt(out);
			for(int i=0; i<size_; i++) {
				data_[i] = data_[i] * (1.0/out);
			}
		}



	} // ----------

	T norm(){
		T out = T(0);
		for(int i=0; i<size_; i++) {
			out = out + conj(data_[i])*data_[i];
		}
		return sqrt(out);
	} // ----------


	Vector<T>& equal(Vector<T>& in) {
		//assert(size_=in.size());
		assert(in.size()>0);
		size_=in.size();
		data_.resize(size_);
		clear();
		for (int i=0;i<size_;i++)
			data_[i] = in(i);
		return *this;
	} // ----------





private:
	int size_;
	std::vector<T> data_;
};



#endif // VECTOR_H

