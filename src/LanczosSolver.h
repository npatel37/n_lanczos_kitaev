#ifndef LANCZOSSOLVER_H
#define LANCZOSSOLVER_H
#include "Hamiltonian.h"


extern "C" void   dstev_(char *,int *, double *, double *, double *, int *, double *, int *);
class LanczosSolver {
public:
	LanczosSolver(ConstVariables& variables, QBasis& Basis, Hamiltonian& Hamil):
	    variables_(variables), Basis_(Basis), Hamil_(Hamil),
	    Nsite_(variables.NumberofSites)
	{

	}

	// -------------------------------------------------------
	VectorXf Lanczos_Nirav(){
		std::cout << "Begin Lanczos! --- "<<std::endl;
		int iter, Lexit;
		double E0, D0, diff;
		int STARTIT=3; //iteration which diagonz. begins
		int MAXiter=variables_.LanczosSteps;
		double LanczosEps=variables_.LanczosEps;


		const int N=Basis_.basis.size();
		VectorXf Vorig(N), V0(N), V1(N), V2(N), Psi(N);
		VectorXf  e(N), d(N);
		vector<double> alpha,beta;
		Matrix<double> TrEVec;

		iter = 0;

		Vorig = VectorXf::Random(N); //	randomize(Vorig);
		Vorig.normalize();			//	normalize(Vorig);

		for (int EViter = 0; EViter < 2; EViter++) {//0=get E0 converge, 1=get eigenvec

			iter = 0;
			V0 = Vorig;

			if (EViter == 1) Psi = V0*TrEVec(0,0);

			V1 = VectorXf::Zero(N);
			beta.push_back(0.0);  //beta_0 not defined

			V1 = MatrixVectorMultXf(V0);		// |V1> = H |V0>
			alpha.push_back(V0.dot(V1));

			if (N==1){
				std::cerr<<"Hilbert space ="<<N<<"\n";
				Lexit = 1;
				std::cout << " Output Energy: " <<setprecision(16)<< alpha[0];
				std::cout << " After " << iter << " iterations " << std::endl;
				Psi[0] = 1.0;
				break;
			}

			V1 = V1 - alpha[0]*V0;	// (Equation 2.3) Rev. Mod. Phys., 66, 3 (1994) - note beta = 0.0
			beta.push_back(V1.norm());

			V1 = V1/beta[1];

			if (EViter == 1) Psi = Psi + V1*TrEVec(1,0);

			// done 0th iteration
			Lexit = 0;   //exit flag
			E0 = 1.0;    //previous iteration GS eigenvalue

			// ------ while ----------------
			while(Lexit != 1){

				iter++;
				V2 = MatrixVectorMultXf(V1); // V2 = H |V1>
				alpha.push_back(V1.dot(V2));

				// (Equation 2.3) Rev. Mod. Phys. , Vol. 66, No. 3, July 1994
				V2 = V2 - alpha[iter]*V1 -  beta[iter]*V0;
				beta.push_back(V2.norm());
				V2 = V2/beta[iter+1];

				if (EViter == 1) Psi = Psi + V2*TrEVec(iter+1,0);
				V0 = V1;
				V1 = V2;

				if (iter > STARTIT && EViter == 0) { // && iter%5==0){

					//diagonalize tri-di matrix
					D0 = Diagonalize_Guangkun('N',alpha,beta,TrEVec);
					diff = abs(E0-D0);
					if(diff < LanczosEps) {
						Lexit = 1;
						E0 = D0;
						std::cout << " Output Energy: " <<setprecision(16)<< D0;
						std::cout << " After " << iter << " iterations "
						          << " Eps: " << diff
						          << std::endl;
					}
					else {
						E0=D0;
					}
					std::cout << iter << " \t " <<setprecision(16)<< D0 <<  " \t "
					          << Psi.norm() << " \t "
					          << " Eps: " << diff
					          << std::endl;


				}//end STARTIT


				if(iter>MAXiter) Lexit=1;

				if (EViter == 1) {
					std::cout << iter << " \t " <<setprecision(16)<< D0 <<  " \t "
					          << Psi.norm() << " \t "
					          << iter << endl;
				}

				if (EViter == 1 && iter == MAXiter) Lexit = 1;
			} // ------ while --------

			if (EViter == 0) {
				D0 = Diagonalize_Guangkun('V',alpha,beta,TrEVec);
				Lexit=1;
			}

			MAXiter = iter;

			std::cout << endl;
			std::cout << "  Lowest Eigen Value: " << D0 << endl;
			std::cout << "  With Lanczos EPS:   " << diff << endl;
			std::cout << "  Ground State Norm:  " << Psi.norm() << std::endl;
		} // EViter
		Psi.normalize();
		//			cout << Psi << endl;
		//			cout << " - --- " << endl;
		//			cout << MatrixVectorMultXf(Psi)/(-2.875948999538671)  << endl;
		std::cout << std::endl;
		//		for (int i=0; i<alpha.size(); i++)	{
		//			std::cout << i << "   " << alpha[i] << "   " << beta[i] << std::endl;

		//		}
		std::cout << std::endl;
		return Psi;
	}

	// -------------------------------------------------------
	// Matrix Vector Multiplication H|vin> = |vout>
	VectorXf MatrixVectorMultXf(const VectorXf& Vin) {
		if(Vin.size()!=Hamil_.HDiag.size()) {
			string errorout ="LanczosSolver.h::Dot-Product size mismatch!";
			throw std::invalid_argument(errorout);
		}

		int hilbert=Vin.size();
		VectorXf Vout;
		Vout = VectorXf::Zero(hilbert); // "Just incase" - set all elements to zero

		// Diagonal Hamiltonian terms!
		for(int i=0;i<hilbert;i++) {
			dcomplex Hij=Hamil_.HDiag[i];
			assert(Hij.imag()==0);
			Vout[i] += Hij.real()*Vin[i];
		}

		int hilbert_t=Hamil_.HTSxx.size();
		// Kxx Kitaev - Sector -------
		for(int i=0;i<hilbert_t;i++){
			int Hi=Hamil_.Sxx_init[i];
			int Hj=Hamil_.Sxx_final[i];
			dcomplex Hij=Hamil_.HTSxx[i];
			assert(Hi<hilbert && Hj<hilbert);
			assert(Hij.imag()==0);
			Vout[Hj] += Hij.real()*Vin[Hi];
//			Vout[Hi] += Hij.real()*Vin[Hj];
		}

		hilbert_t=Hamil_.HTSyy.size();
		// Kyy Kitaev - Sector -------
		for(int i=0;i<hilbert_t;i++){
			int Hi=Hamil_.Syy_init[i];
			int Hj=Hamil_.Syy_final[i];
			dcomplex Hij=Hamil_.HTSyy[i];
			assert(Hi<hilbert && Hj<hilbert);
			assert(Hij.imag()==0);
			Vout[Hj] += Hij.real()*Vin[Hi];
//			Vout[Hi] += Hij.real()*Vin[Hj];
		}

		return Vout;
	}

	// -------------------------------------------------------
	double Diagonalize_Guangkun(char option,
	                            const std::vector<double>& alpha,
	                            const std::vector<double>& beta,
	                            Matrix<double>& Z){
		char jobz=option;
		int N=alpha.size();
		int LDZ = N*5;
		int Nwork= 2*N - 2;
		int info= -1;
		Z.resize(LDZ,N);
		std::vector<double> D, E, work;
		work.resize(Nwork);


		D.resize(N); E.resize(N-1);
		D[0] = alpha[0];
		for(int i = 1; i<N; i++) {
			assert(i<alpha.size());
			D[i] = alpha[i];
			E[i-1] = beta[i];
		}

		// ---- Diagonalize the Tri-diag matrix --------------------
		// dstev_(char *,int *, double *, double *, double *, int *, double *, int *);
		dstev_(&jobz,&N,&(D[0]),&(E[0]),&(Z(0,0)),&LDZ,&(work[0]),&info);
		if (info!=0) {
			std::cerr<<"info="<<info<<"\n";
			perror("diag: dstev: failed with info!=0.\n");
		}

//		for(int i=0; i<=N; i++) {
//			cout << i << " \t " << D[i] << endl;
//		}

		return D[0];
	}

	// -------------------------------------------------------
	double Diagonalize(char option,
	                   const std::vector<double>& alpha,
	                   const std::vector<double>& beta)
	{
		char jobz=option;
		int N=alpha.size();
		int LDZ = N*5;
		int Nwork= 2*N - 2;
		int info= -1;
		Matrix<double> Z(LDZ,N);
		std::vector<double> D, E, work;
		work.resize(Nwork);
		D.resize(N); E.resize(N-1);
		for(int i = 0; i<N; i++) {
			D[i] = alpha[i];
			if(i<N-1) E[i] = sqrt(beta[i+1]);
		}

		// ---- Diagonalize the Tri-diag matrix --------------------
		// dstev_(char *,int *, double *, double *, double *, int *, double *, int *);
		dstev_(&jobz,&N,&(D[0]),&(E[0]),&(Z(0,0)),&LDZ,&(work[0]),&info);
		if (info!=0) {
			std::cerr<<"info="<<info<<"\n";
			perror("diag: dstev: failed with info!=0.\n");
		}
		return D[0];
	}

	// -------------------------------------------------------
	inline void PrintVec(const std::vector<double>& V) {
		for (int i=0; i<V.size(); i++) {
			std::cout<<V[i]<<"\t";
		}
		std::cout<<std::endl;
	}

private:
	ConstVariables& variables_;
	QBasis& Basis_;
	Hamiltonian& Hamil_;
	int Nsite_,Nup_,Ndn_;
};




#endif // LANCZOSSOLVER_H
