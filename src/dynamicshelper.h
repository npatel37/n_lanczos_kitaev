#ifndef DYNAMICSHELPER_H
#define DYNAMICSHELPER_H

#include "DynLanczosSolver.h"
typedef DynLanczosSolver DynLancType;
typedef Observables ObservType;

// ===== define functions ==========
VectorXf ReturnRow(Matrix<double>& mat, int row);
void StartDynamics(string& dynstr,ConstVariables& variables, Lattice& Lat,
                   QBasis& Basis,Hamiltonian& Hamil,
                   DynLancType& DLancGS, Eigen::VectorXcd& Psi);
void DynSzSz(string& dynstr,ConstVariables& variables, Lattice& Lat,
             QBasis& Basis,Hamiltonian& Hamil,
             DynLancType& DLancGS, Eigen::VectorXcd& Psi);
void LocalDynSzSz(string& dynstr,ConstVariables& variables, Lattice& Lat,
                  QBasis& Basis,Hamiltonian& Hamil,
                  DynLancType& DLancGS, Eigen::VectorXcd& Psi);
void DynJsJs(string& dynstr,ConstVariables& Parameters, Lattice& Lat,
             QBasis& Basis, Hamiltonian& Hamiltonian,
             DynLancType& DLancGS, Eigen::VectorXcd& Psi);
void LocalDynRIXS(string& dynstr,ConstVariables& Parameters, Lattice& Lat,
                  QBasis& Basis, Hamiltonian& Hamiltonian,
                  DynLancType& DLancGS, Eigen::VectorXcd& Psi);

dcomplex invComplex(double x, double y){
	double re = x/(x*x+y*y);
	double im = y/(x*x+y*y);
	dcomplex out(re,im);
	return out;
}

// ================================================
// --------- distribute dynamics ------------------
void StartDynamics(string& dynstr, ConstVariables& variables, Lattice& Lat,
                   QBasis& Basis, Hamiltonian& Hamil,
                   DynLancType& DLancGS, Eigen::VectorXcd& Psi) {
    if(dynstr=="SzSz"){
        DynSzSz(dynstr,variables,Lat,Basis,Hamil,DLancGS,Psi);
    } else if(dynstr=="LocalSzSz"){
        LocalDynSzSz(dynstr,variables,Lat,Basis,Hamil,DLancGS,Psi);
    } else if(dynstr=="SpinCurr"){
        DynJsJs(dynstr,variables,Lat,Basis,Hamil,DLancGS,Psi);
    } else if(dynstr=="LocalRIXS"){
        LocalDynRIXS(dynstr,variables,Lat,Basis,Hamil,DLancGS,Psi);
    } else {
        std::cerr<<"dynstr="<<dynstr<<"\n";
        throw std::string("Unknown dynstr parameter \n");
    }
}

// ================================================
// --------- LOCAL - SzSz Dynamics ---- <gs|Siz(t) Siz(0)|gs>
void LocalDynSzSz(string& dynstr,ConstVariables& Parameters, Lattice& Lat,
                  QBasis& Basis, Hamiltonian& Hamiltonian,
                  DynLancType& DLancGS, Eigen::VectorXcd& Psi){
    cout << " --> Startin Dynamics: " << dynstr << endl;
    int Nsite_ = Parameters.NumberofSites;
    int centeral_site = Parameters.SpecialSite;
    cout << setprecision(8);

    int omegasteps=2000;
    double domega=0.005;
    double eta = 0.009;
    double Eg = DLancGS.TriDeval(0);

    ObservType Observ(Parameters, Lat, Basis, Hamiltonian);

    int ExtState = DLancGS.PsiAll.size();
    int someSt = int(ExtState*1.0);
    //cout << ExtState << " \t " << someSt << endl;

    std::vector<Eigen::MatrixXcd> Overlap(3);
    Eigen::VectorXcd Sx(Nsite_), Sy(Nsite_), Sz(Nsite_);
    Overlap[0].resize(Nsite_,ExtState); Overlap[0].setZero();
    Overlap[1].resize(Nsite_,ExtState); Overlap[1].setZero();
    Overlap[2].resize(Nsite_,ExtState); Overlap[2].setZero();

    for (int si=0; si<Nsite_; si++) {
        Eigen::VectorXcd Sxi = Observ.ApplySx(Psi,si);
        Eigen::VectorXcd Syi = Observ.ApplySy(Psi,si);
        Eigen::VectorXcd Szi = Observ.ApplySz(Psi,si);

        Sx[si] = Psi.dot(Sxi);
        Sy[si] = Psi.dot(Syi);
        Sz[si] = Psi.dot(Szi);

        for (int n=0; n<someSt; n++) {
            Eigen::VectorXcd nstate = DLancGS.PsiAll[n];
            Overlap[0](si,n) = nstate.dot(Sxi);// - Sx[si];
            Overlap[1](si,n) = nstate.dot(Syi);// - Sy[si];
            Overlap[2](si,n) = nstate.dot(Szi);// - Sz[si];
        }
    }

    ofstream myfile;
    myfile.open ("2SSLocal_outSpectrum.real");
    Eigen::VectorXcd data(6); data.setZero();

    for (int om=0; om<=omegasteps;om++) {
        double omega = domega*om;
        Eigen::MatrixXcd Itensity(3,3);
        Itensity.setZero();

        dcomplex dnom1(omega,-eta);
        for (int si=0;si<Nsite_;si++) {
            for (int n=0; n<someSt; n++) {

                double En = DLancGS.TriDeval(n);
                dcomplex denominator(omega-(En-Eg),-eta);

                // --- <n|Si|gs> ---
                dcomplex tmpx = Overlap[0](si,n);
                dcomplex tmpy = Overlap[1](si,n);
                dcomplex tmpz = Overlap[2](si,n);

                // --- <gs|S'|n> <n|S|gs> ---
                dcomplex tmpxx = conj(tmpx) * tmpx; // x x
                dcomplex tmpxy = conj(tmpx) * tmpy; // x y
                dcomplex tmpxz = conj(tmpx) * tmpz; // x z

                dcomplex tmpyy = conj(tmpy) * tmpy; // y y
                dcomplex tmpyz = conj(tmpy) * tmpz; // x z
                dcomplex tmpzz = conj(tmpz) * tmpz; // z z

                Itensity(0,0) = Itensity(0,0) + tmpxx/denominator;
                Itensity(0,1) = Itensity(0,1) + tmpxy/denominator;
                Itensity(0,2) = Itensity(0,2) + tmpxz/denominator;

                Itensity(1,1) = Itensity(1,1) + tmpyy/denominator;
                Itensity(1,2) = Itensity(1,2) + tmpyz/denominator;
                Itensity(2,2) = Itensity(2,2) + tmpzz/denominator;

            }

            // --- subtracting background elastic contribution ---
            Itensity(0,0) = Itensity(0,0) - conj(Sx[si])*Sx[si]/dnom1;
            Itensity(0,1) = Itensity(0,1) - conj(Sx[si])*Sy[si]/dnom1;
            Itensity(0,2) = Itensity(0,2) - conj(Sx[si])*Sz[si]/dnom1;
            Itensity(1,1) = Itensity(1,1) - conj(Sy[si])*Sy[si]/dnom1;
            Itensity(1,2) = Itensity(1,2) - conj(Sy[si])*Sz[si]/dnom1;
            Itensity(2,2) = Itensity(2,2) - conj(Sz[si])*Sz[si]/dnom1;
        }

        data(0) = Itensity(0,0);
        data(1) = Itensity(1,1);
        data(2) = Itensity(2,2);
        data(3) = Itensity(0,1);
        data(4) = Itensity(0,2);
        data(5) = Itensity(1,2);

        myfile << omega << " \t ";
        for(int kk=0;kk<6;kk++) myfile << data[kk].real() << " \t " << data[kk].imag() << " \t ";
        myfile << " \n ";

//        cout << omega << " \t ";
//        for(int kk=0;kk<6;kk++) cout << data[kk].real() << " \t " << data[kk].imag() << " \t ";
//        cout << " \n ";
    }
    cout << " " << endl;

    cout << endl;
    myfile.close();
}


// ================================================
// --------- LOCAL - Js.Js Dynamics ---------
void DynJsJs(string& dynstr,ConstVariables& Parameters, Lattice& Lat,
             QBasis& Basis, Hamiltonian& Hamiltonian,
             DynLancType& DLancGS, Eigen::VectorXcd& Psi){
    cout << " --> Startin Dynamics: " << dynstr << endl;

    int Nsite_ = Parameters.NumberofSites;
    int centeral_site = Parameters.SpecialSite;
    cout << setprecision(8);

    int omegasteps=2000;
    double domega=0.005;
    double eta = 0.009;
    double Eg = DLancGS.TriDeval(0);

    ObservType Observ(Parameters, Lat, Basis, Hamiltonian);

    int ExtState = DLancGS.PsiAll.size();
    int someSt = int(ExtState*1.0);
    //cout << ExtState << " \t " << someSt << endl;



    Eigen::VectorXcd Jtx = Observ.ApplyJsxTotal(Psi);
    Eigen::VectorXcd Jty = Observ.ApplyJsyTotal(Psi);
    Eigen::VectorXcd Jtz = Observ.ApplyJszTotal(Psi);

    Eigen::MatrixXcd Overlap(ExtState,3); Overlap.setZero();
    for (int n=0; n<someSt; n++) {
        Eigen::VectorXcd nstate = DLancGS.PsiAll[n];
        Overlap(n,0) = nstate.dot(Jtx);
        Overlap(n,1) = nstate.dot(Jty);
        Overlap(n,2) = nstate.dot(Jtz);
    }

    ofstream myfile;
    myfile.open ("JsJs_outSpectrum.real");

    Eigen::VectorXcd data(6); data.setZero();
    for (int om=0; om<=omegasteps;om++) {
        double omega = domega*om;
        Eigen::MatrixXcd Itensity(3,3);
        Itensity.setZero();

        for (int n=0; n<someSt; n++) {
            double En = DLancGS.TriDeval(n);
            dcomplex denominator(omega-(En-Eg),-eta);

            // --- <n|J|gs> ---
            dcomplex tmpx = Overlap(n,0);
            dcomplex tmpy = Overlap(n,1);
            dcomplex tmpz = Overlap(n,2);


            // --- <gs|J'|n> <n|J|gs> ---
            dcomplex tmpxx = conj(tmpx) * tmpx; // x x
            dcomplex tmpxy = conj(tmpx) * tmpy; // x y
            dcomplex tmpxz = conj(tmpx) * tmpz; // x z

            dcomplex tmpyy = conj(tmpy) * tmpy; // y y
            dcomplex tmpyz = conj(tmpy) * tmpz; // x z
            dcomplex tmpzz = conj(tmpz) * tmpz; // z z

            Itensity(0,0) = Itensity(0,0) + tmpxx/denominator;
            Itensity(0,1) = Itensity(0,1) + tmpxy/denominator;
            Itensity(0,2) = Itensity(0,2) + tmpxz/denominator;

            Itensity(1,1) = Itensity(1,1) + tmpyy/denominator;
            Itensity(1,2) = Itensity(1,2) + tmpyz/denominator;
            Itensity(2,2) = Itensity(2,2) + tmpzz/denominator;
        }

        data(0) = Itensity(0,0);
        data(1) = Itensity(1,1);
        data(2) = Itensity(2,2);
        data(3) = Itensity(0,1);
        data(4) = Itensity(0,2);
        data(5) = Itensity(1,2);

        myfile << omega << " \t ";
        for(int kk=0;kk<6;kk++) myfile << data[kk].real() << " \t " << data[kk].imag() << " \t ";
        myfile << " \n ";

//        cout << omega << " \t ";
//        for(int kk=0;kk<6;kk++) cout << data[kk].real() << " \t " << data[kk].imag() << " \t ";
//        cout << " \n ";
    }
    cout << " " << endl;

    cout << endl;
    myfile.close();
}


// ================================================
// --------- LOCAL - SzSz Dynamics ---- <gs|Siz(t) Siz(0)|gs>
void LocalDynRIXS(string& dynstr,ConstVariables& Parameters, Lattice& Lat,
                  QBasis& Basis, Hamiltonian& Hamiltonian,
                  DynLancType& DLancGS, Eigen::VectorXcd& Psi){
	cout << " ================================================ " << endl;
	cout << " ------- Startin RIXS Local Dynamics " << dynstr << " ------ " << endl;
	cout << " ================================================ " << endl;

	int Nsite_ = Parameters.NumberofSites;
	int centeral_site = Parameters.SpecialSite;
	double Eg = DLancGS.TriDeval(0);
	dcomplex zero(0.0,0.0);

	Hamiltonian.SetupConnectors();
	Hamiltonian.RIXS_killcenter(centeral_site);
	// ================================
	Hamiltonian.TightB_Ham();
	Hamiltonian.Diagonal();
	// ================================

    ObservType Observ(Parameters, Lat, Basis, Hamiltonian);
    //ObservType Observ(Parameters, Basis, Hamiltonian);
    DynLancType DLancInter(Parameters, Basis, Hamiltonian);
	DLancInter.Lanczos_Nirav("All");

	int nExtState = DLancInter.PsiAll.size();
	int mExtState = DLancGS.PsiAll.size();
	cout << nExtState << " \t " << mExtState << endl;
	double eta = 0.1;
	double Gamma = 100;

	Matrix<dcomplex> AI(mExtState,nExtState),ASx(mExtState,nExtState);
	Matrix<dcomplex> ASy(mExtState,nExtState),ASz(mExtState,nExtState);
	AI.fill(zero); ASx.fill(zero); ASy.fill(zero); ASz.fill(zero);
	for (int m=0; m<mExtState; m++) {
		cout << m << " ";
		Eigen::VectorXcd mstate = DLancGS.PsiAll[m];
		int cs = centeral_site;
		dcomplex Idsum(zero), Sxsum(zero), Sysum(zero), Szsum(zero);
		for (int n=0; n<nExtState; n++) {
			double En = DLancInter.TriDeval(n);
			Eigen::VectorXcd nstate = DLancInter.PsiAll[n];
			Eigen::VectorXcd Sxi_ns = Observ.ApplySx(nstate,cs);
			Eigen::VectorXcd Syi_ns = Observ.ApplySy(nstate,cs);
			Eigen::VectorXcd Szi_ns = Observ.ApplySz(nstate,cs);
			dcomplex n0 = nstate.dot(Psi);
			dcomplex mIdn = mstate.dot(nstate);
			dcomplex mSxn = mstate.dot(Sxi_ns);
			dcomplex mSyn = mstate.dot(Syi_ns);
			dcomplex mSzn = mstate.dot(Szi_ns);
			dcomplex inv = invComplex(0-(En),Gamma);
			AI(m,n) = n0*mIdn*inv;
			ASx(m,n) = n0*mSxn*inv;
			ASy(m,n) = n0*mSyn*inv;
			ASz(m,n) = n0*mSzn*inv;
		}
	}
	cout << endl;



	ofstream myfile;
	myfile.open ("RIXS_outSpectrum.real");
	for (int om=0; om<=120;om++) {
		double omega = 0.1*om;

		dcomplex Io(zero), Ix(zero), Iy(zero), Iz(zero);
		for (int m=1; m<mExtState; m++) {
			double Em = DLancGS.TriDeval(m);
			double xm = omega-(Em-Eg);
			double mlorentzian = (eta)/(xm*xm + eta*eta);
			int cs = centeral_site;

			dcomplex Idsum(zero), Sxsum(zero), Sysum(zero), Szsum(zero);
			for (int n=0; n<nExtState; n++) {
				Idsum += AI(m,n);
				Sxsum += ASx(m,n);
				Sysum += ASy(m,n);
				Szsum += ASz(m,n);
			}

			Io += Idsum*conj(Idsum)*mlorentzian;
			Ix += Sxsum*conj(Sxsum)*mlorentzian;
			Iy += Sysum*conj(Sysum)*mlorentzian;
			Iz += Szsum*conj(Szsum)*mlorentzian;
		}
		cout << omega << " \t "
		     << Io.real() << " \t " << Ix.real() << " \t " << Iy.real() << " \t " << Iz.real() << " \t "
		     << Io.imag() << " \t " << Ix.imag() << " \t " << Iy.imag() << " \t " << Iz.imag() << endl;

		myfile << omega << " \t "
		     << Io.real() << " \t " << Ix.real() << " \t " << Iy.real() << " \t " << Iz.real() << " \t "
		     << Io.imag() << " \t " << Ix.imag() << " \t " << Iy.imag() << " \t " << Iz.imag() << endl;
	}
	myfile.close();

}


// ================================================
// --------- SzSz Dynamics ---- <gs|Sz(t) Sz(0)|gs>
void DynSzSz(string& dynstr,ConstVariables& Parameters, Lattice& Lat,
             QBasis& Basis, Hamiltonian& Hamiltonian,
             DynLancType& DLancGS, Eigen::VectorXcd& Psi){
	cout << " ================================================ " << endl;
	cout << " ------- Startin Dynamics " << dynstr << " ------ " << endl;
	cout << " ================================================ " << endl;

	int Nsite_ = Parameters.NumberofSites;
	int corb = 0;
    ObservType Observ(Parameters, Lat, Basis, Hamiltonian);
	DynLancType DLanc(Parameters, Basis, Hamiltonian);
	DLanc.Lanczos_Nirav("All");

	int ExtState = DLanc.PsiAll.size();
	int someSt = int(ExtState*1.0);
	cout << ExtState << " \t " << someSt << endl;

	ofstream myfile;
	myfile.open ("SzSz_outSpectrum.real");
	double eta = 0.1;
	double Eg = DLanc.TriDeval(0);
	for (int om=0; om<=120;om++) {
		double omega = 0.1*om;
		for (int si=0;si<Nsite_;si++) {
			Eigen::VectorXcd Szi = Observ.ApplySz(Psi,si);
			//Szi.normalize();
			for (int sj=0; sj<Nsite_; sj++) {
				double realI=0.0;
				double imagI=0.0;
				Eigen::VectorXcd Szj = Observ.ApplySz(Psi,sj);
				//Szj.normalize();
				for (int n=0; n<someSt; n++) {
					Eigen::VectorXcd nstate = DLanc.PsiAll[n]; // ReturnRow(DLanc.PsiAll, n);
					dcomplex tmp1 = Szj.dot(nstate) * nstate.dot(Szi);
					double tmp = tmp1.real();
					double En = DLanc.TriDeval(n);
					double x = omega-(En-Eg);

					realI += tmp*x/(x*x + eta*eta);
					imagI += tmp*eta/(x*x + eta*eta);
				}
				cout << si << " \t " << sj << " \t " << omega << " \t " << realI << " \t " << imagI << endl;
				myfile << si << " \t " << sj << " \t " << omega << " \t " << realI << " \t " << imagI << endl;
			}
			cout << " " << endl;
		}
	}
	myfile.close();
}




















































#endif // DYNAMICSHELPER_H

