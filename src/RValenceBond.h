#ifndef RVALENCEBOND_H
#define RVALENCEBOND_H
#include "QuantumBasis.h"

typedef QBasis Basistype;

void ApplyToNull(const vector<int> site,
                 const vector<int> spin,
                 Basistype Basis,
                 const int Nsite_,
                 double& signT,
                 int& newup,
                 int& newdn,
                 int& state) {

	int siteSize=site.size();
	int spinSize=spin.size();
	assert(siteSize==spinSize);
	//assert(newup==0 && newdn==0); // has to be null state

	int NupT;
	double signN;
	signN = 1.0;
	signT=signN;

	newup=0; newdn=0; state=0;
	for(int i=siteSize-1; i>-1; i--){
		int updn = spin[i];
		int si = site[i];
		assert(updn==0 || updn==1); // either up or down

		if(updn==1) { //up
			newup = Basis.applyCd(newup,si,signN);
			signT = signT*signN;
		} else if(updn==0) {
			newdn = Basis.applyCd(newdn,si,signN);
			NupT = Basis.NPartInState(newup,Nsite_);
			signT = signT*signN*pow(-1.0,NupT);
		}

	}
}



// --- Create RVB for SU(2) ----- |RVB> -----
VectorXf CreateRVB(Basistype Basis, int Orbitals,
                   int Nsite_, VectorXf Psi,
                   bool giveonly_uudd) {
	int nulup,nuldn,fstate;
	int newup,newdn;
	double signT, signN;
	nulup=0; nuldn=0;
	fstate=0;
	signN = 1.0;
	newup=nulup; newdn=nuldn;
	signT=signN;

	int k_maxup = Basis.upbasis.size();
	int k_maxdn = Basis.downbasis.size();
	int nn=k_maxup*k_maxdn;

	VectorXf RVB; RVB.setZero();

	std::cout << "	size: "<<k_maxup<<"(up) * "<<k_maxdn<<"(down) = "<< k_maxup*k_maxdn << std::endl;

	int up=1, dn=0;
	vector<vector<int>> _s_{{up,up,dn,dn},
		                    {dn,dn,up,up},
		                    {up,dn,up,dn},
		                    {dn,up,dn,up},
		                    {up,dn,dn,up},
		                    {dn,up,up,dn}};
	double tmp=1.0/sqrt(3.0);
	vector<double> coefs1{tmp,tmp,-0.5*tmp,-0.5*tmp,-0.5*tmp,-0.5*tmp};


	int s0,s1,s2,s3;
	s0=0; s1=1; s2=2; s3=3;
	vector<int> site{ s0*Orbitals+0, s0*Orbitals+1, s1*Orbitals+0, s1*Orbitals+1,
		        s2*Orbitals+0, s2*Orbitals+1, s3*Orbitals+0, s3*Orbitals+1};

	VectorXf Sp01Sp23(nn); Sp01Sp23.setZero();
	for(int i=0; i<6; i++){
		for(int j=0; j<6; j++){
			double coef = coefs1[i]*coefs1[j];

			vector<int> spin(8);
			for(int k=0; k<4; k++) spin[k]=_s_[i][k];
			for(int k=0; k<4; k++) spin[k+4]=_s_[j][k];

			ApplyToNull(site,spin,Basis,Nsite_,signT,newup,newdn,fstate);
			assert(Basis.DG(newup,0)!=-1); assert(Basis.DG(newdn,1)!=-1);
			int basisLabel = Basis.DG(newup,0)*k_maxdn + Basis.DG(newdn,1);
			Sp01Sp23[basisLabel] = coef*signT;

			//				cout << i << "--" << j << " \t";
			//				cout << signT <<"  " << coef << " \t"; Basis.PrintBitwise(newup,newdn); cout << endl;
			//				if(j==5) cout << " ----------- " << endl;
		}
	}

	//cout << "singlet overlap 0-1 |X| 2-3 " << Sp01Sp23.dot(Psi) << "    " << Sp01Sp23.norm() << endl;

	s0=1; s1=2; s2=3; s3=0;
	site = {s0*Orbitals+0, s0*Orbitals+1, s1*Orbitals+0, s1*Orbitals+1,
	        s2*Orbitals+0, s2*Orbitals+1, s3*Orbitals+0, s3*Orbitals+1};

	VectorXf Sp12Sp30(nn); Sp12Sp30.setZero();
	for(int i=0; i<6; i++){
		for(int j=0; j<6; j++){
			double coef = coefs1[i]*coefs1[j];

			vector<int> spin(8);
			for(int k=0; k<4; k++) spin[k]=_s_[i][k];
			for(int k=0; k<4; k++) spin[k+4]=_s_[j][k];

			ApplyToNull(site,spin,Basis,Nsite_,signT,newup,newdn,fstate);
			assert(Basis.DG(newup,0)!=-1); assert(Basis.DG(newdn,1)!=-1);
			int basisLabel = Basis.DG(newup,0)*k_maxdn + Basis.DG(newdn,1);
			Sp12Sp30[basisLabel] = coef*signT;

			//				cout << i << "--" << j << " \t";
			//				cout << signT <<"  " << coef << " \t"; Basis.PrintBitwise(newup,newdn); cout << endl;
			//				if(j==5) cout << " ----------- " << endl;
		}
	}

	//cout << "singlet overlap 1-2 |X| 3-0 " << Sp12Sp30.dot(Psi) << "    " << Sp12Sp30.norm() << endl;


	s0=0; s1=2; s2=1; s3=3;
	site = {s0*Orbitals+0, s0*Orbitals+1, s1*Orbitals+0, s1*Orbitals+1,
	        s2*Orbitals+0, s2*Orbitals+1, s3*Orbitals+0, s3*Orbitals+1};

	VectorXf Sp02Sp13(nn); Sp02Sp13.setZero();
	for(int i=0; i<6; i++){
		for(int j=0; j<6; j++){
			double coef = coefs1[i]*coefs1[j];

			vector<int> spin(8);
			for(int k=0; k<4; k++) spin[k]=_s_[i][k];
			for(int k=0; k<4; k++) spin[k+4]=_s_[j][k];

			ApplyToNull(site,spin,Basis,Nsite_,signT,newup,newdn,fstate);
			assert(Basis.DG(newup,0)!=-1); assert(Basis.DG(newdn,1)!=-1);
			int basisLabel = Basis.DG(newup,0)*k_maxdn + Basis.DG(newdn,1);
			Sp02Sp13[basisLabel] = coef*signT;

			//				cout << i << "--" << j << " \t";
			//				cout << signT <<"  " << coef << " \t"; Basis.PrintBitwise(newup,newdn); cout << endl;
			//				if(j==5) cout << " ----------- " << endl;
		}
	}

	//cout << "singlet overlap 0-2 |X| 1-3 " << Sp02Sp13.dot(Psi) << "    " << Sp01Sp23.norm() << endl;
	cout << " overlap < 0-1 X 2-3 | 1-2 X 3-0 > = " << Sp01Sp23.dot(Sp12Sp30) << endl;

	RVB = Sp01Sp23 - Sp12Sp30; // - Sp02Sp13;
	cout << "RVB Norm " << sqrt(RVB.dot(RVB)) << "    " << RVB.norm() << endl;
	RVB = RVB/sqrt(RVB.dot(RVB));
	//cout << "RVB Overlap " << RVB.dot(Psi) << "     " << RVB.norm() << endl;



	//		for(int i=0; i<1000; i++){
	//			double alpha = -1.0 + i*0.001;

	//			VectorXf RVB(nn); RVB.setZero();
	//			RVB = Sp01Sp23 + Sp12Sp30 + alpha*Sp02Sp13;
	//			double norm = RVB.norm();
	//			RVB = RVB/norm;
	//			cout << alpha << " \t " << RVB.dot(Psi) << " \t " << RVB.norm() << endl;
	//		}


	cout << " ------- Variational RVB -------- " << endl;
	RVB.setZero();
	RVB = Sp01Sp23 + Sp12Sp30 + -0.645*Sp02Sp13;
	RVB.normalize();
	cout << "RVB Norm " << sqrt(RVB.dot(RVB)) << "    " << RVB.norm() << endl;
	cout << "RVB Overlap " << RVB.dot(Psi) << "     " << RVB.norm() << endl;

	cout << " ------- keep only uudduudd and dduudduu --------- " << endl;
	VectorXf TEST;
	TEST.setZero();
	if(giveonly_uudd) {
		VectorXf TEST(RVB.size());
		TEST.setZero();
		TEST[9*70+60] = RVB[9*70+60];
		//TEST[60*70+9] = RVB[60*70+9];
		TEST.normalize();
		cout << "uudd Overlap <uudd|psi> " << TEST.dot(Psi) << "    " << TEST.norm() << endl;
		return TEST;
	}

	cout << "RVB6 Norm " << sqrt(RVB.dot(RVB)) << "    " << RVB.norm() << endl;
	//cout << "RVB6 Overlap " << TEST.dot(Psi) << "     " << TEST.norm() << endl;

	return RVB;
}






// --- Create RVB for large Anisotropy ----- |xy> -----
VectorXf CreateXY_RVB(Basistype Basis, int Orbitals,
                   int Nsite_, VectorXf Psi,
                   bool giveonly_uudd) {
	cout << " creating |xy> RVB state " << endl;
	int nulup,nuldn,fstate;
	int newup,newdn;
	double signT, signN;
	nulup=0; nuldn=0;
	fstate=0;
	signN = 1.0;
	newup=nulup; newdn=nuldn;
	signT=signN;

	int k_maxup = Basis.upbasis.size();
	int k_maxdn = Basis.downbasis.size();
	int nn=k_maxup*k_maxdn;

	VectorXf RVB; RVB.setZero();
	std::cout << "	size: "<<k_maxup<<"(up) * "<<k_maxdn<<"(down) = "<< k_maxup*k_maxdn << std::endl;

	int up=1, dn=0;
	vector<vector<int>> _s_{{up,dn,up,dn},
		                    {dn,up,dn,up},
		                    {up,dn,dn,up},
		                    {dn,up,up,dn}};

	double tmp=1.0/sqrt(4.0);
	int Nconf=4;
	vector<double> coefs1{tmp,tmp,tmp,tmp};


	int s0,s1,s2,s3;
	s0=0; s1=1; s2=2; s3=3;
	vector<int> site{ s0*Orbitals+0, s0*Orbitals+1, s1*Orbitals+0, s1*Orbitals+1,
		        s2*Orbitals+0, s2*Orbitals+1, s3*Orbitals+0, s3*Orbitals+1};

	VectorXf Sp01Sp23(nn); Sp01Sp23.setZero();
	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			double coef = coefs1[i]*coefs1[j];

			vector<int> spin(8);
			for(int k=0; k<4; k++) spin[k]=_s_[i][k];
			for(int k=0; k<4; k++) spin[k+4]=_s_[j][k];

			ApplyToNull(site,spin,Basis,Nsite_,signT,newup,newdn,fstate);
			assert(Basis.DG(newup,0)!=-1); assert(Basis.DG(newdn,1)!=-1);
			int basisLabel = Basis.DG(newup,0)*k_maxdn + Basis.DG(newdn,1);
			Sp01Sp23[basisLabel] = coef*signT;

			//				cout << i << "--" << j << " \t";
			//				cout << signT <<"  " << coef << " \t"; Basis.PrintBitwise(newup,newdn); cout << endl;
			//				if(j==5) cout << " ----------- " << endl;
		}
	}

	//------------ 1-2 X 3-0 -------------------
	s0=1; s1=2; s2=3; s3=0;
	site = {s0*Orbitals+0, s0*Orbitals+1, s1*Orbitals+0, s1*Orbitals+1,
	        s2*Orbitals+0, s2*Orbitals+1, s3*Orbitals+0, s3*Orbitals+1};

	VectorXf Sp12Sp30(nn); Sp12Sp30.setZero();
	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			double coef = coefs1[i]*coefs1[j];

			vector<int> spin(8);
			for(int k=0; k<4; k++) spin[k]=_s_[i][k];
			for(int k=0; k<4; k++) spin[k+4]=_s_[j][k];

			ApplyToNull(site,spin,Basis,Nsite_,signT,newup,newdn,fstate);
			assert(Basis.DG(newup,0)!=-1); assert(Basis.DG(newdn,1)!=-1);
			int basisLabel = Basis.DG(newup,0)*k_maxdn + Basis.DG(newdn,1);
			Sp12Sp30[basisLabel] = coef*signT;

			//				cout << i << "--" << j << " \t";
			//				cout << signT <<"  " << coef << " \t"; Basis.PrintBitwise(newup,newdn); cout << endl;
			//				if(j==5) cout << " ----------- " << endl;
		}
	}

	//------------ 0-2 X 1-3 -------------------
	s0=0; s1=2; s2=1; s3=3;
	site = {s0*Orbitals+0, s0*Orbitals+1, s1*Orbitals+0, s1*Orbitals+1,
	        s2*Orbitals+0, s2*Orbitals+1, s3*Orbitals+0, s3*Orbitals+1};

	VectorXf Sp02Sp13(nn); Sp02Sp13.setZero();
	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			double coef = coefs1[i]*coefs1[j];

			vector<int> spin(8);
			for(int k=0; k<4; k++) spin[k]=_s_[i][k];
			for(int k=0; k<4; k++) spin[k+4]=_s_[j][k];

			ApplyToNull(site,spin,Basis,Nsite_,signT,newup,newdn,fstate);
			assert(Basis.DG(newup,0)!=-1); assert(Basis.DG(newdn,1)!=-1);
			int basisLabel = Basis.DG(newup,0)*k_maxdn + Basis.DG(newdn,1);
			Sp02Sp13[basisLabel] = coef*signT;

			//				cout << i << "--" << j << " \t";
			//				cout << signT <<"  " << coef << " \t"; Basis.PrintBitwise(newup,newdn); cout << endl;
			//				if(j==5) cout << " ----------- " << endl;
		}
	}


	cout << "	overlap < 0-1 X 2-3 | 1-2 X 3-0 > = " << Sp01Sp23.dot(Sp12Sp30) << endl;
	cout << "	overlap < 0-1 X 2-3 | 0-2 X 1-3 > = " << Sp02Sp13.dot(Sp01Sp23) << endl;

//	for(int i=0; i<100; i++){
//		double alpha = -10.0 + i*0.2;

//		VectorXf RVB(nn); RVB.setZero();
//		RVB = Sp01Sp23 + Sp12Sp30 + alpha*Sp02Sp13;
//		double norm = RVB.norm();
//		RVB = RVB/norm;
//		cout << alpha << " \t " << RVB.dot(Psi) << " \t " << RVB.norm() << endl;
//	}

	cout << " ------- Variational RVB -------- " << endl;
	RVB.setZero();
	RVB = Sp01Sp23 + Sp12Sp30 + -1.0*Sp02Sp13;
	RVB.normalize();

	cout << "	RVB Norm: " << RVB.norm() << endl;
	cout << "	RVB Overlap " << RVB.dot(Psi) << "     " << RVB.norm() << endl;
	cout << " -------------------------------- " << endl;

	return RVB;
}


















#endif // RVALENCEBOND_H

