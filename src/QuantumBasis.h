#ifndef QUANTUMBASIS_H
#define QUANTUMBASIS_H
#include "QuantumBasis.h"
typedef std::complex<double> dcomplex;


class QBasis {
public:
	QBasis(ConstVariables& variables):
	    variables_(variables), Nsite_(variables.NumberofSites)
	{
		Initialize();
	}

	std::vector<int> basis;

	/*  ***************************************
	 *  ------------- Functions  --------------
	 *  *************************************** */

	// create basis in binary representation
	// 0 = up
	// 1 = down
	void  Initialize(){
		//std::cout << "Building basis for up and down separatly: " << std::endl;
        basis.resize(pow(2,Nsite_));
		for(int ket=0; ket<pow(2,Nsite_); ket++) {
			//PrintBitwise(ket);
            basis[ket] = ket;
		}
		std::cout << "number of ket basis: " << basis.size() << std::endl;
	} // ------------------

	void  ApplySz(const int& site, int& ketin,
	              int& ketout,
	              std::complex<double>& coef){
		coef=std::complex<double>(0.0,0.0);
		ketout=-1;

		int bit = int(findBit(ketin,site));
		coef = (bit==0) ? std::complex<double>(0.5,0.0) : std::complex<double>(-0.5,0.0);;
		ketout=ketin;
		assert(ketout>-1);
	} // ------------------

	void  ApplySx(const int& site, const int& ketin,
	              int& ketout,
	              std::complex<double>& coef){
		coef=std::complex<double>(0.5,0.0); //0.5+0i;
		ketout=-1;
		ketout = flipBit(ketin,site);
	} // ------------------

	void  ApplySy(const int& site, const int& ketin,
	              int& ketout,
	              std::complex<double>& coef){
		coef=std::complex<double>(0,0); //0.+0.i;
		ketout=-1;
		int bit = int(findBit(ketin,site));
		ketout = flipBit(ketin,site);
		coef = (bit==0) ? std::complex<double>(0.0,0.5): std::complex<double>(0.0,-0.5); //0.+0.5i : 0.-0.5i;
	} // ------------------


	/*  ********************************************
		Here we create a mask, apply the mask to n,
		and then right shift the masked value to get just
		the bit we want. We could write it out more fully as:
		*
		int mask =  1 << position;  			create mask
		int masked_pos = i & position;			apply mask to i
		int thebit = masked_pos >> position;	right shift the masked value to get bit at position
	*/
	int findBit(const int i, const int position){
		return (i & ( 1 << position )) >> position;
	} // ---------------

	int flipBit(const int ketin, int position){
		return ketin^(1<<position);
	} // ---------------

	int hoppstate(int initial_state,int hopp_from,int hopp_to) {
		assert(findBit(initial_state,hopp_from)==1);
		assert(findBit(initial_state,hopp_to)==0);

		int mask = (1 << hopp_from) + (1 << hopp_to);
		return mask^initial_state;
	}

	void PrintBitwise(int i){
		for(int j=0;j<Nsite_;j++){
			//std::cout << ((i & ( 1 << j )) >> j) << "  ";
			std::cout << findBit(i,j) << "  ";
		}
		std::cout << std::endl;
	} // ---------------

	int NPartInState(int state, int Nsite_){
		int particles=0;
		for(int i=0; i<Nsite_; i++){
			particles += findBit(state,i);
		}
		return particles;
	}

private:
	ConstVariables& variables_;
	int Nsite_;
};



#endif // QUANTUMBASIS_H
