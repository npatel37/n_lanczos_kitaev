#ifndef OBSERVABLES_H
#define OBSERVABLES_H

typedef std::complex<double> dcomplex;
typedef Eigen::VectorXf VectorXf;

extern "C" void   dstev_(char *,int *, double *, double *, double *, int *, double *, int *);
class Observables {
public:
    Observables(ConstVariables& variables, Lattice& Lat, QBasis& Basis, Hamiltonian& Hamil):
        variables_(variables), Lat_(Lat), Basis_(Basis), Hamil_(Hamil)
    {

    }

    // -----------------------------------
    Eigen::VectorXcd ApplyOp(Eigen::VectorXcd& inV, const string OpA, const int& site) {
        int Nsite_ = variables_.NumberofSites;
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out;

        if (OpA == "Sz") {
           return ApplySz(inV, site);
        } else if (OpA == "Sy") {
           return ApplySy(inV, site);
        } else if (OpA == "Sx") {
            return ApplySx(inV, site);
        } else if (OpA == "I") {
            return inV;
        } else if (OpA == "Jsx") {
           return ApplyJsx(inV, site);
        } else if (OpA == "Jsy") {
            return ApplyJsy(inV, site);
        } else if (OpA == "Jsz") {
            return ApplyJsz(inV, site);
        } else {
            std::cerr<<"Operator="<<OpA<<" does not exist \n";
            throw std::string("Unknown Operator label \n");
        }
    }

    // -----------------------------------
    void measureLocalJs(Eigen::VectorXcd& GS_) {
        int Nsite_ = variables_.NumberofSites;
        cout << "\n-------- measuring <Js_xi>, <Js_yi>, <Js_zi> --------" << endl;
        for(int si=0; si<Nsite_; si++) {
            Eigen::VectorXcd tmpx = ApplyJsx(GS_, si);
            Eigen::VectorXcd tmpy = ApplyJsy(GS_, si);
            Eigen::VectorXcd tmpz = ApplyJsz(GS_, si);
            cout << si << " \t " << GS_.dot(tmpx) << " \t " << GS_.dot(tmpy) << " \t " << GS_.dot(tmpz) << endl;
        }
    }

    // -----------------------------------
    void measureLocalS(Eigen::VectorXcd& GS_) {
        int Nsite_ = variables_.NumberofSites;

        cout << "\n-------- measuring <sx>, <sy>, <sz> --------" << endl;
        for(int si=0; si<Nsite_; si++) {
            Eigen::VectorXcd tmpx = ApplySx(GS_,si);
            Eigen::VectorXcd tmpy = ApplySy(GS_,si);
            Eigen::VectorXcd tmpz = ApplySz(GS_,si);
            cout << si << " \t " << GS_.dot(tmpx) << " \t " << GS_.dot(tmpy) << " \t " << GS_.dot(tmpz) << endl;
        }
    }

    // -----------------------------------
    Eigen::MatrixXcd TwoPointCorr(const string OpA, const string OpB, Eigen::VectorXcd& GS_) {
        //cout << " -------- " << endl;
        std::cout << "Calculating " << OpA << "." << OpB << " correlations " << std::endl;
        int Nsite_ = variables_.NumberofSites;
        int Hsize = Basis_.basis.size();
        Eigen::MatrixXcd Corr(Nsite_,Nsite_);
        dcomplex dczero(0.0,0.0);

        // #pragma omp parallel for
        for(int sj=0; sj<Nsite_; sj++) {
            Eigen::VectorXcd out1(Hsize);
            out1.setZero();
            out1 = ApplyOp(GS_, OpB, sj);
            for(int si=0; si<Nsite_; si++) {
                if(sj>=si) {
                    Eigen::VectorXcd out2(Hsize);
                    out2.setZero();
                    out2 = ApplyOp(out1, OpA, si);
                    Corr(si,sj) = GS_.dot(out2);
                    Corr(sj,si) = Corr(si,sj);
                } else {
                    Corr(si,sj) = dczero;
                }
            }
        }
        //Corr.print();
        return Corr;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplyJsz(Eigen::VectorXcd& GS_, const int& si) {
        int Nsite_ = variables_.NumberofSites;
        assert(si<Nsite_);
        int xneigh = Lat_.N1neigh_(si,2); // - x neigh
        int yneigh = Lat_.N1neigh_(si,1); // - y neigh
        int zneigh = Lat_.N1neigh_(si,0); // - z neigh

        assert(zneigh<Nsite_); // z - neigh
        assert(yneigh<Nsite_); // y - neigh
        assert(xneigh<Nsite_); // x - neigh

        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd outL1(Hsize); outL1.setZero();
        Eigen::VectorXcd outL2(Hsize); outL2.setZero();
        Eigen::VectorXcd outR1(Hsize); outR1.setZero();
        Eigen::VectorXcd outR2(Hsize); outR2.setZero();
        Eigen::VectorXcd outB1(Hsize); outB1.setZero();
        Eigen::VectorXcd outB2(Hsize); outB2.setZero();

        // Sx_i . Sy_i+y
        outL1 = ApplyOp(GS_, "Sy", yneigh);
        outL2 = ApplyOp(outL1, "Sx", si);
        outL2 = variables_.Kyy*outL2;

        // Sy_i . Sx_i+x
        outR1 = ApplyOp(GS_, "Sx", xneigh);
        outR2 = ApplyOp(outR1, "Sy", si);
        outR2 = variables_.Kxx*outR2;

        // Sy_i
        outB1 = ApplyOp(GS_, "Sy", si);
        outB1 = variables_.Bxx*outB1;

        // Sx_i
        outB2 = ApplyOp(GS_, "Sx", si);
        outB2 = variables_.Byy*outB2;

        return outL2 - outR2 - outB1 + outB2;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplyJsy(Eigen::VectorXcd& GS_, const int& si) {
        int Nsite_ = variables_.NumberofSites;
        assert(si<Nsite_);
        int xneigh = Lat_.N1neigh_(si,2); // - x neigh
        int yneigh = Lat_.N1neigh_(si,1); // - y neigh
        int zneigh = Lat_.N1neigh_(si,0); // - z neigh

        assert(zneigh<Nsite_); // z - neigh
        assert(yneigh<Nsite_); // y - neigh
        assert(xneigh<Nsite_); // x - neigh

        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd outL1(Hsize); outL1.setZero();
        Eigen::VectorXcd outL2(Hsize); outL2.setZero();
        Eigen::VectorXcd outR1(Hsize); outR1.setZero();
        Eigen::VectorXcd outR2(Hsize); outR2.setZero();
        Eigen::VectorXcd outB1(Hsize); outB1.setZero();
        Eigen::VectorXcd outB2(Hsize); outB2.setZero();

        // Sz_i . Sx_i+x
        outL1 = ApplyOp(GS_, "Sx", xneigh);
        outL2 = ApplyOp(outL1, "Sz", si);
        outL2 = variables_.Kxx*outL2;

        // Sx_i . Sz_i+z
        outR1 = ApplyOp(GS_, "Sz", zneigh);
        outR2 = ApplyOp(outR1, "Sx", si);
        outR2 = variables_.Kzz*outR2;

        // Sz_i
        outB1 = ApplyOp(GS_, "Sz", si);
        outB1 = variables_.Bxx*outB1;

        // Sx_i
        outB2 = ApplyOp(GS_, "Sx", si);
        outB2 = variables_.Bzz*outB2;

        return outL2 - outR2 + outB1 - outB2;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplyJsx(Eigen::VectorXcd& GS_, const int& si) {
        int Nsite_ = variables_.NumberofSites;
        assert(si<Nsite_);
        int xneigh = Lat_.N1neigh_(si,2); // - x neigh
        int yneigh = Lat_.N1neigh_(si,1); // - y neigh
        int zneigh = Lat_.N1neigh_(si,0); // - z neigh

        assert(zneigh<Nsite_); // z - neigh
        assert(yneigh<Nsite_); // y - neigh
        assert(xneigh<Nsite_); // x - neigh

        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd outL1(Hsize); outL1.setZero();
        Eigen::VectorXcd outL2(Hsize); outL2.setZero();
        Eigen::VectorXcd outR1(Hsize); outR1.setZero();
        Eigen::VectorXcd outR2(Hsize); outR2.setZero();
        Eigen::VectorXcd outB1(Hsize); outB1.setZero();
        Eigen::VectorXcd outB2(Hsize); outB2.setZero();

        // Syi . Sz_i+z
        outL1 = ApplyOp(GS_, "Sz", zneigh);
        outL2 = ApplyOp(outL1, "Sy", si);
        outL2 = variables_.Kzz*outL2;

        // Sz_i . Sy_i+y
        outR1 = ApplyOp(GS_, "Sy", yneigh);
        outR2 = ApplyOp(outR1, "Sz", si);
        outR2 = variables_.Kyy*outR2;

        // Sz_i
        outB1 = ApplyOp(GS_, "Sz", si);
        outB1 = variables_.Byy*outB1;

        // Sy_i
        outB2 = ApplyOp(GS_, "Sy", si);
        outB2 = variables_.Bzz*outB2;

        return outL2 - outR2 - outB1 + outB2;
    }


    // -----------------------------------
    Eigen::VectorXcd ApplySz(Eigen::VectorXcd& GS_, const int& i) {
        int Nsite_ = variables_.NumberofSites;
        assert(i<Nsite_);
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out(Hsize); out.setZero();

        for(int i1=0; i1<Hsize; i1++) {
            int ket = Basis_.basis[i1];
            int keto1;
            dcomplex Szi;

            Basis_.ApplySz(i,ket,keto1,Szi);
            assert(ket==keto1);
            out(keto1) += Szi*GS_(ket);
        }
        return out;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplySy(Eigen::VectorXcd& GS_, const int& i) {
        int Nsite_ = variables_.NumberofSites;
        assert(i<Nsite_);
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out(Hsize); out.setZero();

        for(int i1=0; i1<Hsize; i1++) {
            int ket = Basis_.basis[i1];
            int keto1;
            dcomplex Szi;

            Basis_.ApplySy(i,ket,keto1,Szi);
            out(keto1) += Szi*GS_(ket);
        }
        return out;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplySx(Eigen::VectorXcd& GS_, const int& i) {
        int Nsite_ = variables_.NumberofSites;
        assert(i<Nsite_);
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out(Hsize); out.setZero();

        for(int i1=0; i1<Hsize; i1++) {
            int ket = Basis_.basis[i1];
            int keto1;
            dcomplex Szi;

            Basis_.ApplySx(i,ket,keto1,Szi);
            out(keto1) += Szi*GS_(ket);
        }
        return out;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplySzTotal(Eigen::VectorXcd& GS_, bool square) {
        int Nsite_ = variables_.NumberofSites;
        int Hsize = Basis_.basis.size();

        Eigen::VectorXcd out(Hsize);
        for(int i1=0; i1<Hsize; i1++) {
            int ket = Basis_.basis[i1];
            int keto1;
            dcomplex Szt=0;

            for (int s=0; s<Nsite_;s++) {
                dcomplex Szi;
                Basis_.ApplySz(s,ket,keto1,Szi);
                assert(ket==keto1);
                assert(Szi.imag()==0);
                Szt+=Szi;
            }
            if(square) {
                out(ket) = Szt*Szt*GS_(ket);
            } else {
                out(ket) = Szt*GS_(ket);
            }

        }
        return out;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplySxTotal(Eigen::VectorXcd& GS_, bool square) {
        int Nsite_ = variables_.NumberofSites;
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out(Hsize);
        out.setZero();

        for (int s=0; s<Nsite_;s++) {
            out += ApplySx(GS_,s);
        }
        return out;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplySyTotal(Eigen::VectorXcd& GS_, bool square) {
        int Nsite_ = variables_.NumberofSites;
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out(Hsize);
        out.setZero();

        for (int s=0; s<Nsite_;s++) {
            out += ApplySy(GS_,s);
        }
        return out;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplyJsxTotal(Eigen::VectorXcd& GS_) {
        int Nsite_ = variables_.NumberofSites;
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out(Hsize);
        out.setZero();

        for (int s=0; s<Nsite_;s++) {
            out += ApplyJsx(GS_, s);
        }
        return out;
    }
    // -----------------------------------
    Eigen::VectorXcd ApplyJsyTotal(Eigen::VectorXcd& GS_) {
        int Nsite_ = variables_.NumberofSites;
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out(Hsize);
        out.setZero();

        for (int s=0; s<Nsite_;s++) {
            out += ApplyJsy(GS_, s);
        }
        return out;
    }

    // -----------------------------------
    Eigen::VectorXcd ApplyJszTotal(Eigen::VectorXcd& GS_) {
        int Nsite_ = variables_.NumberofSites;
        int Hsize = Basis_.basis.size();
        Eigen::VectorXcd out(Hsize);
        out.setZero();

        for (int s=0; s<Nsite_;s++) {
            out += ApplyJsz(GS_, s);
        }
        return out;
    }

private:
    ConstVariables& variables_;
    Lattice& Lat_;
    QBasis& Basis_;
    Hamiltonian& Hamil_;
    //VectorXf& GS_;
};
#endif // OBSERVABLES_H
