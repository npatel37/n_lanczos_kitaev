/* ***************************************************
 *  Authors:Chris Bishop & Nirav D. Patel
 *	Data:January 14, 2016
 *	Platform: linux
 *  Language: C++
 *	Model: 3OrMC_MF-Test
 * ***************************************************
*/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <omp.h>
#include <vector>
#include <cstdlib>
#include <string>
#include <stdexcept>
#include <random>
#include <complex>
#include <cmath>
#include <cassert>
#include <eigen3/Eigen/Dense>
#include <eigen3/unsupported/Eigen/CXX11/Tensor>

using namespace std;
//#include "Vector.h"
#include "Memusage.h"
#include "Matrix.h"
#include "ParametersEngine.h"
#include "Lattice.h"
#include "QuantumBasis.h"
#include "Hamiltonian.h"
#include "Exactdiag.h"
#include "LanczosSolver.h"
#include "Observables.h"
//#include "RValenceBond.h"
#include "dynamicshelper.h"


typedef Eigen::VectorXf VectorXf;
typedef ConstVariables ConstVariablestype;
typedef QBasis Basistype;
typedef Hamiltonian HamilType;
typedef LanczosSolver LancType;
typedef DynLanczosSolver DynLancType;
typedef Observables ObservType;

typedef std::complex<double> dcomp;    // your typedef
void PrintGSConf(Eigen::VectorXcd& Psi, Basistype& Basis, int& Nsite_);
//void PrintGSConf(VectorXf& Psi, Basistype& Basis, int& Nsite_, int& basisLabel1);

int main(int argc, char *argv[]) {
	if (argc<2) {
		throw std::invalid_argument("USE:: executable inputfile");
	}

	string inputfile = argv[1];
	double pi=acos(-1.0);
    double vm, rss;
    dcomplex zero(0.0,0.0);

	//std::cout.unsetf ( std::ios::floatfield );
	//std::cout.precision(6);

	// Read from the inputfile
	ConstVariablestype Parameters(inputfile);
	omp_set_dynamic(0);
	omp_set_num_threads(Parameters.Threads);
	Lattice Lat(Parameters);
	int Nsite_ = Parameters.NumberofSites;


	// Build binary basis
	Basistype Basis(Parameters);
    int nn=Basis.basis.size();
    process_mem_usage(vm, rss);
    std::cout << "       VM (MB): " << int(vm/1024) << "       RSS (MB): " << int(rss/1024) << std::endl;


	// Build non-zero Diagonal and Tight-binding part of Hamiltonian
	HamilType Hamiltonian(Parameters,Lat,Basis);
	Hamiltonian.TightB_Ham();
    process_mem_usage(vm, rss);
    std::cout << "       VM (MB): " << int(vm/1024) << "       RSS (MB): " << int(rss/1024) << std::endl;

    Hamiltonian.Diagonal();
    process_mem_usage(vm, rss);
    std::cout << "       VM (MB): " << int(vm/1024) << "       RSS (MB): " << int(rss/1024) << std::endl;


    ObservType Observ(Parameters, Lat, Basis, Hamiltonian);
	if(Parameters.Solver=="ED") {
		ExactDiag(Parameters, Basis, Hamiltonian, Observ);
		exit(1);
	}


	// Run Lanczos
	DynLancType Lanczos(Parameters, Basis, Hamiltonian);

	//LancType Lanczos(Parameters,Basis,Hamiltonian);
    Eigen::VectorXcd Psi = Lanczos.Lanczos_Nirav(Parameters.LancType);
	//PrintGSConf(Psi,Basis,Nsite_);
	//for(int i=0;i<Psi.size();i++) { cout << Psi[i] << " \t ";} cout << endl;

	//exit(1);
	cout << " Ritz values: ";
	for(int i=0;i<Lanczos.TriDeval.size();i++) cout << Lanczos.TriDeval[i] << " ";
	cout << endl;


//  cout << " <psi|H|psi> = " << Psi.dot(Hamiltonian.MatrixVectorMult(Psi)) << endl;
    Observ.measureLocalS(Psi);
    Observ.measureLocalJs(Psi);
//	cout << endl;
    //exit(1);

    /*
	Matrix<dcomplex> A, B, C;
    A = Observ.TwoPointCorr("Sx","Sx",Psi); A.print();cout << endl;
    B = Observ.TwoPointCorr("Sy","Sy",Psi); B.print();cout << endl;
    C = Observ.TwoPointCorr("Sz","Sz",Psi); C.print();cout << endl;

	dcomplex sum(0.0,0.0);
	Matrix<dcomplex> SiSj(Nsite_,Nsite_);
	for (int i=0; i<Nsite_; i++){ for (int j=i; j<Nsite_; j++) {
			if(i==j) sum += A(i,j) + B(i,j)+C(i,j);
			if(i!=j) sum += 2.0*(A(i,j) + B(i,j) + C(i,j));
			SiSj(i,j) = A(i,j)+B(i,j)+C(i,j);
		}}
	cout << " Total S(S+1) = " << sum << endl;
	cout << " Total Spin-Spin Correlations " << endl;
	SiSj.print();

	vector<double> kqx{0,pi/4,pi/2,3*pi/4,pi};
	Eigen::VectorXcd Skx(5); Skx.setZero(); //clear();
	dcomplex norm = std::complex<double>(Nsite_*2,0.0); //double(Nsite_*2)+0.i;
	for (int i=0; i<Nsite_; i++){
		for (int j=i; j<Nsite_; j++) {
			int R = i - j;
			Skx(0) += SiSj(i,j)*cos(kqx[0]*R)/norm;
			Skx(1) += SiSj(i,j)*cos(kqx[1]*R)/norm;
			Skx(2) += SiSj(i,j)*cos(kqx[2]*R)/norm;
            Skx(3) += SiSj(i,j)*cos(kqx[3]*R)/norm;
			Skx(4) += SiSj(i,j)*cos(kqx[4]*R)/norm;
		}
	}

	cout << endl;
	cout << Skx(0) << " \t " << Skx(1) << " \t " <<
	        Skx(2) << " \t " << Skx(3) << " \t " <<
	        Skx(4) << endl << endl;
    */

    std::vector<Eigen::MatrixXcd> A(18);
    A[0] = Observ.TwoPointCorr("Sx","Sx",Psi); cout << A[0] << endl << endl;
    A[1] = Observ.TwoPointCorr("Sx","Sy",Psi); cout << A[1] << endl << endl;
    A[2] = Observ.TwoPointCorr("Sx","Sz",Psi); cout << A[2] << endl << endl;
    A[3] = Observ.TwoPointCorr("Sy","Sx",Psi); cout << A[3] << endl << endl;
    A[4] = Observ.TwoPointCorr("Sy","Sy",Psi); cout << A[4] << endl << endl;
    A[5] = Observ.TwoPointCorr("Sy","Sz",Psi); cout << A[5] << endl << endl;
    A[6] = Observ.TwoPointCorr("Sz","Sx",Psi); cout << A[6] << endl << endl;
    A[7] = Observ.TwoPointCorr("Sz","Sy",Psi); cout << A[7] << endl << endl;
    A[8] = Observ.TwoPointCorr("Sz","Sz",Psi); cout << A[8] << endl << endl;




    A[9] = Observ.TwoPointCorr("Jsx","Jsx",Psi); cout << A[9] << endl << endl;
    A[10] = Observ.TwoPointCorr("Jsx","Jsy",Psi); cout << A[10] << endl << endl;
    A[11] = Observ.TwoPointCorr("Jsx","Jsz",Psi); cout << A[11] << endl << endl;
    A[12] = Observ.TwoPointCorr("Jsy","Jsx",Psi); cout << A[12] << endl << endl;
    A[13] = Observ.TwoPointCorr("Jsy","Jsy",Psi); cout << A[13] << endl << endl;
    A[14] = Observ.TwoPointCorr("Jsy","Jsz",Psi); cout << A[14] << endl << endl;
    A[15] = Observ.TwoPointCorr("Jsz","Jsx",Psi); cout << A[15] << endl << endl;
    A[16] = Observ.TwoPointCorr("Jsz","Jsy",Psi); cout << A[16] << endl << endl;
    A[17] = Observ.TwoPointCorr("Jsz","Jsz",Psi); cout << A[17] << endl << endl;

    cout << " --> Spin and Jspin total = \n";
    for(int i=0;i<=17; i++) cout << A[i].sum() << " ";
    cout << endl << endl;

    cout << " --> Spin and Jspin trace = \n";
    for(int i=0;i<=17; i++) cout << A[i].trace() << " ";
    cout << endl << endl;

    cout << A[0] + A[4] + dcomplex(0,1)*A[1] + dcomplex(0,1)*A[3] << endl;


	//exit(1);

    if (Parameters.LocalSkw){
        cout << " ==================================================================== " << endl;
        cout << "                            Dynamical Correlations		           " << endl;
        cout << " ==================================================================== " << endl;
        string str = "LocalSzSz";
        StartDynamics(str,Parameters,Lat,Basis,Hamiltonian,Lanczos,Psi);
    }

    if (Parameters.SpinCurrDynamics){
        cout << " ==================================================================== " << endl;
        cout << "               Spin Current Dynamical Correlations					   " << endl;
        cout << " ==================================================================== " << endl;
        string str1 = "SpinCurr";
        StartDynamics(str1,Parameters,Lat,Basis,Hamiltonian,Lanczos,Psi);
    }

    if (Parameters.RIXSDynamics){
    cout << " ==================================================================== " << endl;
    cout << "                   RIXS Dynamical Correlations						   " << endl;
    cout << " ==================================================================== " << endl;
        string str1 = "LocalRIXS";
        StartDynamics(str1,Parameters,Lat,Basis,Hamiltonian,Lanczos,Psi);
    }

	cout << "--------THE END--------" << endl;
}


/*=======================================================================
 * ======================================================================
 * ======================================================================
*/



void PrintGSConf(Eigen::VectorXcd& Psi, Basistype& Basis, int& Nsite_) {
	int k_maxup = Basis.basis.size();
	int n=k_maxup;

	vector<pair<double,int> >V;
	for(int i=0;i<n;i++){
		pair<double,int>P=make_pair((Psi[i]*conj(Psi[i])).real(),i);
		V.push_back(P);
	}
	sort(V.begin(),V.end());

	for(int i0=n-1;i0>=n-10;i0--){
		int i = V[i0].second;
		int basisLabel1 = i; // i1*k_maxdn + i2;
		int ket = Basis.basis[i];
		double coef = V[i0].first; //Psi[basisLabel1];
		dcomplex coef1 = Psi[basisLabel1];

		cout << basisLabel1 << " - ";
		//if(coef*coef>0.02) {
		for(int s=0;s<Nsite_;s++){
			cout << " \\underline{";
			if(Basis.findBit(ket,s) == 1) {
				cout << " \\uparrow ";
			} else {
				cout << " \\downarrow ";
			}
			cout << "} \\ \\ ";
			if(s==Nsite_-1) cout << " \\ \\ ";
		}
		cout << "& \\ \\ ";
		cout << coef << " \\\\ " << coef1 << endl;
	}
	cout << "   " << endl;

}

/*

void PrintGSConf(VectorXf& Psi, Basistype& Basis, int& Nsite_, int& basisLabel1) {
	int k_maxup = Basis.upbasis.size();
	int k_maxdn = Basis.downbasis.size();
	int n=k_maxup*k_maxdn;

	for(int i1=0; i1<k_maxup; i1++) {
		int up_ket1 = Basis.upbasis[i1];
		for(int i2=0; i2<k_maxdn; i2++) {
			int dn_ket1 = Basis.downbasis[i2];
			int basisLabel = i1*k_maxdn + i2;

			if(basisLabel==basisLabel1) {
				double coef = Psi[basisLabel1];

				cout << basisLabel1 << " - ";
				//if(coef*coef>0.02) {
				for(int s=0;s<Nsite_;s++){
					cout << " \\underline{";
					if(Basis.findBit(up_ket1,s) == 1) {
						cout << " \\uparrow ";
					} else {
						cout << " \\ ";
					}
					if(Basis.findBit(dn_ket1,s) == 1) {
						cout << " \\downarrow ";
					} else {
						cout << " \\ ";
					}
					cout << "} \\ \\ ";
					if(s==Nsite_-1) cout << " \\ \\ ";
				}
				cout << "& \\ \\ ";

//				if(coef<0) {
//					cout << " (-) \\ ";
//				} else {
//					cout << " (+) \\ ";
//				}
				cout << coef << " \\\\ " << endl;
			}
		}
	}

	//}
	//		}}
	cout << "   " << endl;

}




void LaTexPrint(int& upstate, int& dnstate, double& coef, int& Nsite_, Basistype& Basis) {
	for(int s=0;s<Nsite_;s++){
		cout << " \\underline{";
		if(Basis.findBit(upstate,s) == 1) {
			cout << " \\uparrow ";
		} else {
			cout << " \\ ";
		}
		if(Basis.findBit(dnstate,s) == 1) {
			cout << " \\downarrow ";
		} else {
			cout << " \\ ";
		}
		cout << "} \\ \\ ";
		if(s==Nsite_-1) cout << " \\ \\ ";
	}
	cout << "& \\ \\ ";

	if(coef<0) {
		cout << " (-) \\ ";
	} else {
		cout << " (+) \\ ";
	}

	cout << coef*coef << " \\\\ " << endl;

}



*/





/*
-
-
-	{
-		cout << " ==================================================================== " << endl;
-		cout << "                            Finite Temperature						   " << endl;
-		cout << " ==================================================================== " << endl;
-		int Nsite_ = Parameters.NumberofSites;
-		Eigen::VectorXcd phi0 = Lanczos.Vorig;
-
-		double EvTemp_Scale=1; //11605;
-		int ExtState = Lanczos.PsiAll.size();
-		int Nst = int(ExtState*1.0-1);
-		int M = Nst;
-		cout << Nst << " \t " << endl;
-		cout << " energy span: " << Lanczos.TriDeval[M] - Lanczos.TriDeval[0] << endl;
-
-
-		ofstream myfile;
-		myfile.open ("EvsTemp1.dat");
-		// -- partition function --- J. Jaklic and P. Prelovsek - page 14
-
-		Eigen::VectorXcd Hoverlap(M), Hsqoverlap(M), jstOlap(M);
-		Eigen::VectorXcd SxToverlap(M), SyToverlap(M), SzToverlap(M);
-		Eigen::VectorXcd SxTToverlap(M), SyTToverlap(M), SzTToverlap(M);
-
-		Eigen::VectorXcd Hphi0 = Hamiltonian.MatrixVectorMult(phi0);
-		Eigen::VectorXcd HHphi0 = Hamiltonian.MatrixVectorMult(Hphi0);
-		Eigen::VectorXcd Sxphi0 = Observ.ApplySx(phi0,Parameters.SpecialSite);
-		Eigen::VectorXcd Syphi0 = Observ.ApplySy(phi0,Parameters.SpecialSite);
-		Eigen::VectorXcd Szphi0 = Observ.ApplySz(phi0,Parameters.SpecialSite);
-
-		Eigen::VectorXcd SxTotalp0 = Observ.ApplySx(phi0,0);
-		Eigen::VectorXcd SyTotalp0 = Observ.ApplySy(phi0,0);
-		Eigen::VectorXcd SzTotalp0 = Observ.ApplySz(phi0,0);
-		for(int i=1; i<Nsite_;i++) {
-			SxTotalp0 = SxTotalp0 + Observ.ApplySx(phi0,i);
-			SyTotalp0 = SyTotalp0 + Observ.ApplySy(phi0,i);
-			SzTotalp0 = SzTotalp0 + Observ.ApplySz(phi0,i);
-		}
-
-		//Observ.ApplySzTotal(phi0,false);
-		//Szphi0 += Observ.ApplySz(phi0,Parameters.SpecialSite+1);
-		//Eigen::VectorXcd SzT_phi0 = Observ.ApplySzTotal(Szphi0,false);
-
-#pragma omp parallel for
-		for (int j=0; j<M; j++){
-			Hoverlap[j] = Lanczos.PsiAll[j].dot(Hphi0);
-			Hsqoverlap[j] = Lanczos.PsiAll[j].dot(HHphi0);
-
-			SxToverlap[j] = Lanczos.PsiAll[j].dot(Sxphi0);
-			SyToverlap[j] = Lanczos.PsiAll[j].dot(Syphi0);
-			SzToverlap[j] = Lanczos.PsiAll[j].dot(Szphi0);
-
-			SxTToverlap[j] = Lanczos.PsiAll[j].dot(SxTotalp0);
-			SyTToverlap[j] = Lanczos.PsiAll[j].dot(SyTotalp0);
-			SzTToverlap[j] = Lanczos.PsiAll[j].dot(SzTotalp0);
-
-			jstOlap[j] = phi0.dot(Lanczos.PsiAll[j]);
-			cout << j << "    ";
-			cout.flush();
-			//cout << r_jOlap << " \t " << J_HrOlap << " \t " << J_HrOlap*r_jOlap << endl;
-		}
-
-
-		int temax=1000000;
-		double dT=0.00002;
-		double Temp=dT;
-		for(int te=0;te<=temax;te++) {
-			//double Temp = Parameters.Temperature/11605;
-			//double Temp = 0.000002 + te*0.000002;
-			if(Temp>0.05 && Temp<=1) dT=0.0005;
-			if(Temp>1 && Temp<=5) dT=0.005;
-			if(Temp>5) dT=0.05;
-			Temp = Temp + dT;
-			double beta = 1.0/Temp;
-
-			double Energy=0,  Partition=0, Ensq=0, SxT=0, SyT=0, SzT=0;
-			double SxTT=0, SyTT=0, SzTT=0;
-			double expon;
-#pragma omp parallel for private(expon) reduction(+:Partition,Energy,Ensq,SxT,SyT,SzT,SxTT,SyTT,SzTT)
-			for(int j=0; j<M; j++) {
-				expon = exp(-1.0*beta*(Lanczos.TriDeval[j] - Lanczos.TriDeval[0]));
-				Partition+= (expon*jstOlap[j]*jstOlap[j]).real();
-				Energy+= (expon*jstOlap[j]*Hoverlap[j]).real();
-				Ensq+= (expon*jstOlap[j]*Hsqoverlap[j]).real();
-
-				SxT+= (expon*jstOlap[j]*SxToverlap[j]).real();
-				SyT+= (expon*jstOlap[j]*SyToverlap[j]).real();
-				SzT+= (expon*jstOlap[j]*SzToverlap[j]).real();
-
-				SxTT+= (expon*jstOlap[j]*SxTToverlap[j]).real();
-				SyTT+= (expon*jstOlap[j]*SyTToverlap[j]).real();
-				SzTT+= (expon*jstOlap[j]*SzTToverlap[j]).real();
-			}
-
-			double Cv = (Ensq/Partition - (Energy/Partition)*(Energy/Partition))/(Temp*Temp);
-
-			if(te%100==0) {
-				cout << Temp*EvTemp_Scale << " \t " << Partition
-				     << " \t " << Energy
-				     << " \t " << Energy/(Partition*Nsite_)
-				     << " \t " << Ensq/Partition
-				     << " \t " << Cv/Nsite_
-				     << " \t " << (SxT/Partition)
-				     << " \t " << (SyT/Partition)
-				     << " \t " << (SzT/Partition)
-				     << " \t " << (SxTT/Partition)
-				     << " \t " << (SyTT/Partition)
-				     << " \t " << (SzTT/Partition)
-				     << endl;
-			}
-
-			myfile << Temp*EvTemp_Scale << " \t " << Partition
-			       << " \t " << Energy
-			       << " \t " << Energy/(Partition*Nsite_)
-			       << " \t " << Ensq/Partition
-			       << " \t " << Cv/Nsite_
-			       << " \t " << (SxT/Partition)
-			       << " \t " << (SyT/Partition)
-			       << " \t " << (SzT/Partition)
-			       << " \t " << (SxTT/Partition)
-			       << " \t " << (SyTT/Partition)
-			       << " \t " << (SzTT/Partition)
-			       << endl;
-
-			if(Temp*EvTemp_Scale>100) break;
-		}
-		myfile.close();
-		cout << endl;
-
-		// =============== spin - susceptibility ======================
-		cout << " ==================================================================== " << endl;
-		cout << "                            spin - susceptibility					   " << endl;
-		cout << " ==================================================================== " << endl;
-
-		Eigen::MatrixXcd BSz(M,M), BSx(M,M), BSy(M,M);
-		BSz.setZero();
-		BSx.setZero();
-		BSy.setZero();
-
-		for (int i=0; i<M; i++){
-#pragma omp parallel for
-			for (int j=0; j<M; j++) {
-				BSx(i,j) = Lanczos.PsiAll[i].dot(Observ.ApplySx(Lanczos.PsiAll[j],0));
-				BSy(i,j) = Lanczos.PsiAll[i].dot(Observ.ApplySy(Lanczos.PsiAll[j],0));
-				BSz(i,j) = Lanczos.PsiAll[i].dot(Observ.ApplySz(Lanczos.PsiAll[j],0));
-			}
-			cout << i << "    ";
-			cout.flush();
-			//cout << r_jOlap << " \t " << J_HrOlap << " \t " << J_HrOlap*r_jOlap << endl;
-		}
-
-
-		Eigen::MatrixXcd ASz(M,Nsite_), ASx(M,Nsite_), ASy(M,Nsite_);
-		ASz.setZero();
-		ASx.setZero();
-		ASy.setZero();
-
-		for(int s=0; s<Nsite_; s++) {
-			VectorXcd tmpx = Observ.ApplySx(phi0,s);
-			VectorXcd tmpy = Observ.ApplySy(phi0,s);
-			VectorXcd tmpz = Observ.ApplySz(phi0,s);
-
-#pragma omp parallel for
-			for (int j=0; j<M; j++) {
-				ASx(j,s) = Lanczos.PsiAll[j].dot(tmpx);
-				ASy(j,s) = Lanczos.PsiAll[j].dot(tmpy);
-				ASz(j,s) = Lanczos.PsiAll[j].dot(tmpz);
-			}
-
-		}
-
-
-		myfile.open ("SzSzvsTemp.dat");
-		temax=1000000;
-		dT=0.00002;
-		Temp=dT;
-		for(int te=0;te<=temax;te++) {
-			if(Temp>0.05 && Temp<=1) dT=0.0005;
-			if(Temp>1 && Temp<=5) dT=0.005;
-			if(Temp>5) dT=0.05;
-			Temp = Temp + dT;
-			double beta = 1.0/Temp;
-
-
-
-			if(te%10==0) {
-				double SxT=0, SyT=0, SzT=0;
-				double expon;
-
-#pragma omp parallel for private(expon) reduction(+:SxT,SyT,SzT)
-				for(int s=0; s<Nsite_;s++) {
-					for(int i=0; i<M; i++) {
-						expon = exp(-1.0*beta*(Lanczos.TriDeval[i] - Lanczos.TriDeval[0]));
-						for(int j=0; j<M; j++) {
-							SxT+= (expon*jstOlap[j]*BSx(i,j)*ASx(j,s)).real();
-							SyT+= (expon*jstOlap[j]*BSy(i,j)*ASy(j,s)).real();
-							SzT+= (expon*jstOlap[j]*BSz(i,j)*ASz(j,s)).real();
-						}
-					}
-				}
-
-				double Partition=0;
-#pragma omp parallel for private(expon) reduction(+:Partition)
-				for(int j=0; j<M; j++) {
-					expon = exp(-1.0*beta*(Lanczos.TriDeval[j] - Lanczos.TriDeval[0]));
-					Partition+= (expon*jstOlap[j]*jstOlap[j]).real();
-				}
-
-				if(te%100==0) {
-					cout << Temp*EvTemp_Scale << " \t " << Partition
-					     << " \t " << (SxT/Partition)
-					     << " \t " << (SyT/Partition)
-					     << " \t " << (SzT/Partition)
-					     << " \t " << ((SxT+SyT+SzT)/Partition)
-					     << endl;
-				}
-
-				myfile << Temp*EvTemp_Scale << " \t " << Partition
-				       << " \t " << (SxT/Partition)
-				       << " \t " << (SyT/Partition)
-				       << " \t " << (SzT/Partition)
-				       << " \t " << ((SxT+SyT+SzT)/Partition)
-				       << endl;
-
-
-			}
-
-			if(Temp*EvTemp_Scale>100.0) break;
-		}
-		myfile.close();
-
-
-
-
-
-
-		exit(1);

*/







